const appConfig = {
    prefix: 'mz',
    layout: {
        sideMenu: {
            width: 250,
        }
    }
}

export default appConfig;