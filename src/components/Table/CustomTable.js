import React, { useRef, useState, useEffect } from "react";
import { Table, Tooltip } from "antd";
import { saveTableState } from "../../utils/localStorage";
import SearchInput from "./SearchInput";
import "./customtable.less";
// https://ant.design/components/pagination/#API
// https://ant.design/components/table/#API

////// { showAll: true, showPagination: false }

const CustomTable = ({
	showAll = true,
	showFilter = true,
	showPagination = false,
	onInteract = () => undefined,
	prefix = "", // Used to save table state into local storage.
	state: { pageSize = 10, page = 1, loading = false, totalSize = 1, sort = null, order = "ascend" } = {},
	columns = [],
	columnFilters: columnFilterKeys = [],
	...props
}) => {
	const [ height, setHeight ] = useState(1);
	const [ headerHeight, setHeaderHeight ] = useState(0);
	const [ filterRowHeight, setFilterRowHeight ] = useState(0);
	const [ searchValue, setSearchValue ] = useState("");
	const [ columnFilters, setColumnFilters ] = useState({});

	const tableWrapper = useRef();
	const headerRow = useRef();
	const filterRow = useRef();

	useEffect(() => {
		setTimeout(() => {
			if (tableWrapper?.current?.offsetHeight) {
				setHeight(tableWrapper.current.offsetHeight);
			}
		});
	}, []);

	useEffect(() => {
		const tableHeader = headerRow.current;
		const headerFilterRow = filterRow.current;

		if (tableHeader) {
			new ResizeObserver(() => {
				const newHeaderHeight = tableHeader.getBoundingClientRect()?.height ?? 0;
				if (newHeaderHeight) {
					setHeaderHeight(newHeaderHeight + 1);
				}
			}).observe(tableHeader);
		}

		if (headerFilterRow) {
			new ResizeObserver(() => {
				const newFilterRowHeight = headerFilterRow.getBoundingClientRect()?.height ?? 0;
				if (newFilterRowHeight) {
					setFilterRowHeight(newFilterRowHeight + 1);
				}
			}).observe(headerFilterRow);
		}
	}, [headerRow?.current, filterRow?.current]);

	useEffect(() => {
		const updatedColumnFilters = {};
		const currentKeys = [];
		for (const key in columnFilters) {
			currentKeys.push(key);
		}
		let hasDifference = false;
		for (const currentKey of currentKeys) {
			if (!columnFilterKeys.includes(currentKey)) {
				hasDifference = true;
			}
		}
		if (hasDifference) {
			if (columnFilterKeys === 'all') {
				for (const col of columns) {
					updatedColumnFilters[col.key] = columnFilters[col.key] || '';
				}
			} else {
				columnFilterKeys.map(key => {
					updatedColumnFilters[key] = columnFilters[key] || '';
				})
			}
			setColumnFilters(updatedColumnFilters);
		}
	}, [ columnFilterKeys ]);

	const paginationConfig = showAll
		? false
		: {
			total: totalSize,
			showQuickJumper: true,
			// showSizeChange: true,
			pageSize: showAll ? totalSize : pageSize,
			current: page,
			hideOnSinglePage: true,
			pageSizeOptions: ["10", "20", "50", "100"],
			onChange(pageNumber, size) {
				saveTableState(prefix, "page", pageNumber);
				saveTableState(prefix, "size", size);
				handleInteract({
					page: pageNumber,
					pageSize: size,
				});
			},
			...props.pagination,
		};

	const paginationHeight = paginationConfig ? 64 : 0;
	const searchInputHeight = showFilter ? 60 : 0;
	const tableHeight = height - paginationHeight - headerHeight - searchInputHeight - filterRowHeight;

	const handleSearchChange = (value) => {
		setSearchValue(value);
	};

	const onSearch = (value) => {
		handleInteract({
			search: value,
			page: 1,
		});
	};

	const handleInteract = (updatedStates = {}) => {
		onInteract({
			page,
			pageSize,
			search: searchValue,
			sort,
			order,
			columnFilters,
			...updatedStates,
		});
	};

	const handleTableChange = (pagination, filters, sorter, extra) => {
		if (extra?.action === "sort") {
			const { columnKey = null, order = "ascend" } = sorter;
			handleInteract({
				sort: columnKey,
				order,
				page: 1,
			});
		}
	};

	const handleColumnFilterChange = (key, value) => {
		setColumnFilters({
			...columnFilters,
			[key]: value,
		})
	}

	const handleFilterInputKeyDown = event => {
		if (event.key === 'Enter') {
			handleInteract({
				page: 1,
			})
		}
	}

	const handleFilterInputBlur = () => {
		// handleInteract({
		// 	page: 1,
		// })
	}

	const tableColumns = columns.map((col) => {
		let sortOrder = false;
		if (sort === col.dataIndex) {
			sortOrder = order;
		}

		const children = columnFilterKeys === 'all' || columnFilterKeys.includes(col.key)
		?	[
			{
				dataIndex: col.key,
				title: (
					<input
						style={{ width: '100%' }}
						value={columnFilters[col.key] || ''}
						onChange={e => handleColumnFilterChange(col.key, e.target.value)}
						onKeyDown={handleFilterInputKeyDown}
						onBlur={handleFilterInputBlur}
					/>
				)
			  },
		]
		:	columnFilterKeys.length
			?	[
				{
					dataIndex: col.key,
					title: null
				  },
			]
			:	 undefined;

		return {
			...col,
			sorter: col.sorter === false ? false : true,
			sortOrder,
			ellipsis: true,
			showSorterTooltip: true,
			className: 'table-cell',
			title: (
				<Tooltip title={col.name ?? '-'}>
					<div style={{textOverflow: 'ellipsis', overflow: 'hidden'}}>{col.name || null}</div>
				</Tooltip>
			),
			children,
		};
	});

	return (
		<div ref={tableWrapper} style={{ flex: 1 }}>
			{
				showFilter && (
					<div>
						<SearchInput value={searchValue} onChange={handleSearchChange} onSearch={onSearch} />
					</div>
				)
			}
			<Table
				loading={loading}
				columns={tableColumns}
				onChange={handleTableChange}
				onHeaderRow={(columns, index) => {
					const ref = index === 0 ? headerRow : filterRow;
					return {
						ref,
					}
				}}
				size='small'
				{...props}
				pagination={paginationConfig}
				scroll={{ x: true, y: tableHeight }}
			/>
		</div>
	);
};

export default CustomTable;
