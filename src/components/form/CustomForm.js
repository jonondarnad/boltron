import React, { useEffect, useState } from "react";
import { Form, Select, Input, Switch, Button, Row, InputNumber, DatePicker, Upload } from "antd";
import moment from "moment";
import "./customform.less";
import { UploadOutlined } from "@ant-design/icons";

const CustomForm = ({ fields = [], onCancel = () => undefined, onSubmit = () => undefined }) => {
	const [form] = Form.useForm();

	// const [changedSecureKeys, setChangedSecureKeys] = useState([]);

	useEffect(() => {
		const values = {};
		for (const field of fields) {
			if (field.value) {
				let value = field.value;
				if (field.instance === "date") {
					value = moment(field.value);
				}
				values[field.key] = value;
			}
		}
		form.setFieldsValue(values);
	}, [fields]);

	const getFormItem = (field, index) => {
		const formProps = {
			key: index,
			name: field.key,
			label: field.name,
			rules: [
				{
					required: field.instance !== "boolean" && !!field.required,
					message: `${field.name} дутуу байна!`,
				},
			],
		};
		const disabled = field.editLock || field.editable === false;

		if (field.hasOwnProperty("enum")) {
			// This means field is Dropdown/Select
			const options = [...field.enum];
			return (
				<Form.Item {...formProps}>
					<Select disabled={disabled} mode={field.instance === "array" ? "multiple" : undefined}>
						{options.map((option) => {
							return (
								<Select.Option key={option} value={option}>
									{field.enumDictionary?.[option] ?? option}
								</Select.Option>
							);
						})}
					</Select>
				</Form.Item>
			);
		}
		switch (field.instance) {
			case "string": {
				return (
					<Form.Item {...formProps}>
						<Input disabled={disabled} />
					</Form.Item>
				);
			}
			case "boolean": {
				return (
					<Form.Item {...formProps} valuePropName="checked" initialValue={true}>
						<Switch />
					</Form.Item>
				);
			}
			case "number": {
				return (
					<Form.Item {...formProps}>
						<InputNumber disabled={disabled} />
					</Form.Item>
				);
			}
			case "date": {
				return (
					<Form.Item {...formProps}>
						<DatePicker disabled={disabled} />
					</Form.Item>
				);
			}
			case "image": {
				return (
					<Form.Item {...formProps} valuePropName="fileList" getValueFromEvent={normFile}>
						<Upload listType="picture" beforeUpload={() => false} multiple={!!field.multiple} maxCount={field.multiple ? 10 : 1}>
							<Button icon={<UploadOutlined />}>Зураг хуулах</Button>
						</Upload>
					</Form.Item>
				);
			}
			case "custom": {
				return <Form.Item {...formProps}>{field.component}</Form.Item>;
			}
			default:
				return null;
		}
	};

	const getFields = () => {
		const fieldItems = [];
		for (const [index, field] of fields.entries()) {
			if (field.show !== false) {
				fieldItems.push(getFormItem(field, index));
			}
		}
		return fieldItems;
	};

	const handleSubmit = (values) => {
		onSubmit(values);
	};

	const normFile = (e) => {
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const handleFormValueChange = (changedValues) => {
		// const changedKeys = Object.keys(changedValues);
		// const secureKeys = fields.filter(field => field.secure).map(field => field.key);
		// const csKeys = [];
		// for (const changedKey of changedKeys) {
		//     if (secureKeys.includes(changedKey)) {
		//         csKeys.push(changedKey);
		//     }
		// }
		// if (csKeys.length) {
		//     const newChangedKeys = [];
		//     for (const key of csKeys) {
		//         if (!changedSecureKeys.includes(key)) {
		//             newChangedKeys.push(key);
		//         }
		//     }
		//     if (newChangedKeys.length) {
		//         setChangedSecureKeys([...changedSecureKeys, ...newChangedKeys]);
		//         const updatedValue = form.getFieldsValue();
		//         for (const key of newChangedKeys) {
		//             updatedValue[key] = changedValues[key];
		//         }
		//         form.setFieldsValue(updatedValue);
		//     }
		// }
	};

	return (
		<>
			<Form form={form} labelCol={{ span: 8 }} wrapperCol={{ span: 8 }} onFinish={handleSubmit} onValuesChange={handleFormValueChange} autoComplete="off" className="custom-form">
				{getFields()}
				<Row type="flex" justify="center" align="middle">
					<Button style={{ marginRight: 3 }} onClick={onCancel} type="link">
						Буцах
					</Button>
					<Button htmlType="submit" style={{ marginLeft: 3 }} type="primary">
						Хадгалах
					</Button>
				</Row>
			</Form>
		</>
	);
};

export default CustomForm;
