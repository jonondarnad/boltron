export const getFormattedResponse = (data) => {
	if (!data) {
		return {
			list: [],
			columsn: [],
			rest: {},
			raw: data,
		};
	}
	const { result = {} } = data;
	const { docs = [], meta = {}, ...rest } = result;
	const list = docs.map((item) => {
		const itemObject = {};
		for (const key in item) {
			let value = item.hasOwnProperty(key) ? item[key] : null;
			if (value && meta?.[key]?.enum_dictionary?.hasOwnProperty(value)) {
				value = meta[key].enum_dictionary[value];
			} else if (value && meta?.[key]?.enumDictionary?.hasOwnProperty(value)) {
				value = meta[key].enumDictionary[value];
			}
			itemObject[key] = value;
		}
		return itemObject;
	});
	const columns = [];
	for (const key in meta) {
		if (meta.hasOwnProperty(key)) {
			columns.push({
				...meta[key],
				dataIndex: key,
				title: meta[key]?.name ?? "-",
			});
		}
	}
	return {
		list,
		columns,
		rest,
		raw: data,
	};
};
